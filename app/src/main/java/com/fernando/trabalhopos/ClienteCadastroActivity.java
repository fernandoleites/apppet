package com.fernando.trabalhopos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fernando.trabalhopos.bd.ClienteDao;
import com.fernando.trabalhopos.model.Cliente;

public class ClienteCadastroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_cadastro);
        //Log.i("Banco de Dados","Teste: "+"Teste");
    }

    public void cadastrar(View v){
        EditText editTextNomeCliente = (EditText) findViewById(R.id.editTextNomeCliente);
        EditText editTextEndereco = (EditText) findViewById(R.id.editTextEndereco);
        EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        EditText editTextTelefone = (EditText) findViewById(R.id.editTextTelefone);
        Log.i("Banco de Dados","Teste: "+"Teste");

        Cliente cliente = new Cliente(editTextNomeCliente.getText().toString(),
                                        editTextEndereco.getText().toString(),
                                        editTextEmail.getText().toString(),
                                        editTextTelefone.getText().toString());
        ClienteDao dao = new ClienteDao(this);
        dao.inserir(cliente);

        Toast.makeText(this,"Cadastro realizada com sucesso!",Toast.LENGTH_SHORT)
                .show();
        finish();
    }
}
