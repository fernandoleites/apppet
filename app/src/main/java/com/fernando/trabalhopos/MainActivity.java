package com.fernando.trabalhopos;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView clienteList;
    private ImageView petList;
    private ImageView caixa;
    private ImageView service;
    private ImageView agenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        clienteList = (ImageView) findViewById(R.id.imageViewClienteList);
        petList = (ImageView) findViewById(R.id.imageViewPetList);
        caixa = (ImageView) findViewById(R.id.imageViewCaixa);
        service = (ImageView) findViewById(R.id.imageViewService);
        agenda = (ImageView) findViewById(R.id.imageViewAgenda);

        clienteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TesteActivity.class));
            }
        });

        petList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PetListActivity.class));
            }
        });

        caixa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CaixaActivity.class));
            }
        });

        service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ServiceActivity.class));
            }
        });

        agenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AgendaActivity.class));
            }
        });

    }
}
