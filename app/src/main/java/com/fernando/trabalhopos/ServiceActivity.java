package com.fernando.trabalhopos;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.fernando.trabalhopos.model.Servico;

import java.util.ArrayList;
import java.util.List;

public class ServiceActivity extends AppCompatActivity {

    private EditText textoServico;
    private Button botaoCadastrar;
    private ListView listaServicos;
    //private SQLiteDatabase bancoDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        textoServico = (EditText) findViewById(R.id.editTextServico);
        botaoCadastrar = (Button) findViewById(R.id.botaoCadastrar);
        listaServicos = (ListView) findViewById(R.id.listaServicos);

        String[] dados = new String[] { "Banho Pequeno", "Banho Médio", "Banho Grande", "Tosa Bebe", "Tosa total"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dados);
        listaServicos.setAdapter(adapter);



    }
    private List<Servico> gerarServicos() {
        List<Servico> servicos = new ArrayList<Servico>();
        servicos.add(criarServico("Banho"));
        servicos.add(criarServico("Banho Grande"));
        servicos.add(criarServico("Tosa Higienica"));
        return servicos;
    }

    private Servico criarServico(String nome) {
        Servico servico = new Servico(nome);
        return servico;
    }
}
