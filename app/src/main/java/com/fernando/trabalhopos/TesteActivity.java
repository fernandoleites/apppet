package com.fernando.trabalhopos;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.fernando.trabalhopos.adapter.ClienteAdapter;
import com.fernando.trabalhopos.bd.ClienteDao;
import com.fernando.trabalhopos.model.Cliente;

import java.util.ArrayList;
import java.util.List;

public class TesteActivity extends AppCompatActivity {
    List<Cliente> listaClientes = new ArrayList<>();
    ClienteAdapter adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adaptador = new ClienteAdapter(listaClientes,
                this);

        ListView listView = (ListView) findViewById(R.id.id_cliente_list);
        listView.setAdapter(adaptador);

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Cliente cliente = listaClientes.get(position);

                        Intent it = new Intent(TesteActivity.this, TesteActivity.class);
                        it.putExtra("cliente",cliente);
                        startActivity(it);
                    }
                }
        );

        String[] dados = new String[] { "Fernando Leites", "Ana Claudia", "Fulano de Tal", "Roberto"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dados);
        listView.setAdapter(adapter);

        //ArrayAdapter<Cliente> adapter = new ArrayAdapter<Cliente>(this, android.R.layout.simple_list_item_1, listaClientes);
        //listView.setAdapter(adaptador);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TesteActivity.this, ClienteCadastroActivity.class));
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        ClienteDao dao = new ClienteDao(this);
        listaClientes = dao.listar();
        adaptador.setListaClientes(listaClientes);
        adaptador.notifyDataSetChanged();
    }
}
