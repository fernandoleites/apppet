package com.fernando.trabalhopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fernando.trabalhopos.R;
import com.fernando.trabalhopos.model.Cliente;

import java.util.List;

/**
 * Created by fernando_zeferino on 26/05/2017.
 */

public class ClienteAdapter extends BaseAdapter{

    private List<Cliente> listaClientes;
    private Context contexto;

    public ClienteAdapter(List<Cliente> listaClientes, Context contexto) {
        this.listaClientes = listaClientes;
        this.contexto = contexto;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    @Override
    public int getCount() {

        return listaClientes.size();
    }

    @Override
    public Object getItem(int position) {

        return listaClientes.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Cliente cliente = listaClientes.get(position);

        LayoutInflater inflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_teste,null);

        //TextView textoNome = (TextView) view.findViewById(R.id.nome_list);
        //textoNome.setText(cliente.getNome_cliente());

        //TextView textoTelefone = (TextView) view.findViewById(R.id.telefone_list);
        //textoTelefone.setText(cliente.getTelefone());

        return view;
    }
}
