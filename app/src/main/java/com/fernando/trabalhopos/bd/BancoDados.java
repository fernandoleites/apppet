package com.fernando.trabalhopos.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by fernando_zeferino on 26/05/2017.
 */

class BancoDados extends SQLiteOpenHelper{
    private static String nomeBD = "trabalhopos.db";
    private static String createTableServico = "CREATE TABLE IF NOT EXISTS servico" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "servico VARCHAR(30))";
    private static String createTablePet = "CREATE TABLE IF NOT EXISTS pet" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nome_pet VARCHAR(30))";
    private static String createTableCliente = "CREATE TABLE IF NOT EXISTS cliente" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nome_cliente VARCHAR(30)," +
            "endereco VARCHAR(80)," +
            "telefone VARCHAR(80)," +
            "email VARCHAR(80))";
    private static String createTableCaixa = "CREATE TABLE IF NOT EXISTS caixa" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "id_servico INTEGER," +
            "id_cliente INTEGER," +
            "valor REAL," +
            "dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
    private static String createTableAgenda = "CREATE TABLE IF NOT EXISTS agenda" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "id_pet INTEGER," +
            "id_servico INTEGER," +
            "data_servico TIMESTAMP," +
            "hora VARCHAR(6))";

    public BancoDados(Context context) {
        super(context, nomeBD, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTableServico);
        db.execSQL(createTablePet);
        db.execSQL(createTableCliente);
        db.execSQL(createTableCaixa);
        db.execSQL(createTableAgenda);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE contato");
        db.execSQL(createTableServico);
    }


}
