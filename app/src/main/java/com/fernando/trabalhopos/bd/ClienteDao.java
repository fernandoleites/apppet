package com.fernando.trabalhopos.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fernando.trabalhopos.model.Cliente;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fernando_zeferino on 26/05/2017.
 */

public class ClienteDao {
    private BancoDados bdOpenHelper;

    public ClienteDao(Context contexto){
        bdOpenHelper = new BancoDados(contexto);
    }

    public void inserir(Cliente cliente){
        SQLiteDatabase banco = bdOpenHelper.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put("nome_cliente", cliente.getNome_cliente());
        valores.put("endereco", cliente.getEndereco());
        valores.put("email", cliente.getEmail());
        valores.put("telefone", cliente.getTelefone());

        banco.close();
    }

    public List<Cliente> listar() {
        SQLiteDatabase banco = bdOpenHelper.getReadableDatabase();

        Cursor cursor = banco.query("cliente",
                new String[]{"id","nome_cliente","endereco","email","telefone"},
                null,null,null,null,null);

        List<Cliente> listaClientes = new ArrayList<>();

        while(cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String nome_cliente = cursor.getString(cursor.getColumnIndex("nome_cliente"));
            String endereco = cursor.getString(cursor.getColumnIndex("endereco"));
            String email = cursor.getString(cursor.getColumnIndex("email"));
            String telefone = cursor.getString(cursor.getColumnIndex("telefone"));
            Cliente cliente = new Cliente(id, nome_cliente, endereco, email, telefone);
            listaClientes.add(cliente);
        }
        return listaClientes;
    }

    public Cliente procurarPorId(int id) {
        return null;
    }
}
