package com.fernando.trabalhopos.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.fernando.trabalhopos.model.Servico;

/**
 * Created by iossenac on 27/05/17.
 */

public class ServicoDao {

    private BancoDados bdOpenHelper;

    public ServicoDao(Context contexto){
        bdOpenHelper = new BancoDados(contexto);
    }

    public void inserir(Servico servico){
        SQLiteDatabase banco = bdOpenHelper.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put("servico", servico.getNome());

        banco.close();
    }
}
