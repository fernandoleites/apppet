package com.fernando.trabalhopos.model;

import java.io.Serializable;

/**
 * Created by fernando_zeferino on 26/05/2017.
 */

public class Cliente implements Serializable {

    private int id;
    private String nome_cliente;
    private String endereco;
    private String email;
    private String telefone;

    public Cliente(String nome_cliente, String endereco, String email, String telefone) {
        this.nome_cliente = nome_cliente;
        this.endereco = endereco;
        this.email = email;
        this.telefone = telefone;
    }

    public Cliente(int id, String nome_cliente, String endereco, String email, String telefone) {
        this.id = id;
        this.nome_cliente = nome_cliente;
        this.endereco = endereco;
        this.email = email;
        this.telefone = telefone;
    }

    public String getNome_cliente() {
        return nome_cliente;
    }

    public void setNome_cliente(String nome_cliente) {
        this.nome_cliente = nome_cliente;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }


}
